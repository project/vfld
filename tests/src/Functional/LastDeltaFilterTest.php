<?php

namespace Drupal\Tests\vfld\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\views\Functional\ViewTestBase;

/**
 * Tests the functionality of the Last Delta Views filter.
 *
 * @group vfld
 */
class LastDeltaFilterTest extends ViewTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'vfld_test_view',
    'node',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  public static array $testViews = ['views_filter_last_delta_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views, ['vfld_test_view']);

    // Create a user.
    $admin_permissions = ['access content'];
    $account = $this->drupalCreateUser($admin_permissions);
    $this->drupalLogin($account);

    // Create a content type.
    $this->createContentType(['type' => 'article']);

    // Add a field with unlimited cardinality to the content type.
    $field_name = 'field_test_multivalue';
    FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'text',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'article',
      'label' => 'Test Multivalue Field',
    ])->save();

    // Create node with two values.
    $this->createNode([
      'type' => 'article',
      'field_test_multivalue' => ['One', 'Two'],
    ]);

    // Create node with one value.
    $this->createNode([
      'type' => 'article',
      'field_test_multivalue' => ['Three'],
    ]);
  }

  /**
   * Tests the Last Delta.
   */
  public function testLastDeltaFilter() {
    // Assert the View is ready.
    $this->drupalGet('views-filter-last-delta-test');
    $this->assertSession()->statusCodeEquals(200);

    // Assert that unfiltered page contains all the deltas.
    $this->assertSession()->pageTextContains('One');
    $this->assertSession()->pageTextContains('Two');
    $this->assertSession()->pageTextContains('Three');
    $this->assertSession()->elementsCount('css', '.views-row', 3);

    // Enable the Views Filter Last Delta.
    $this->enableLastDeltaFilter();

    // Assert the View is ready.
    $this->drupalGet('views-filter-last-delta-test');
    $this->assertSession()->statusCodeEquals(200);

    // Assert that filtered page does contain just the last delta.
    $this->assertSession()->pageTextNotContains('One');
    $this->assertSession()->pageTextContains('Two');
    $this->assertSession()->pageTextContains('Three');
    $this->assertSession()->elementsCount('css', '.views-row', 2);
  }

  /**
   * Enables the Views Filter Last Delta in the test View.
   */
  protected function enableLastDeltaFilter() {
    $view = $this->container->get('entity_type.manager')
      ->getStorage('view')
      ->load('views_filter_last_delta_test');
    $displays = $view->get('display');
    $displays['default']['display_options']['filters']['delta_vfld']['value'] = 1;
    $view->set('display', $displays);
    $view->save();
    \Drupal::service('cache.data')->deleteAll();
  }

}
