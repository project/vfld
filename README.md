# Views Filter Last Delta Drupal Module

## Overview

The Views Filter Last Delta module addresses a unique need when working with
multi-value fields in Drupal. Often, the last value entered in a multi-value
field is of particular importance and may be the only piece of information you
want to display in an entity View.

Drupal's Views module provides two modes for displaying multi-value fields:

1. All values in the same row
2. Each value in an individual row

However, if you only want to output the last row containing the last value of
the field, the Views Filter Last Delta module is for you.

## Usage

To use the Views Filter Last Delta module, follow these steps:

1. Enable the module as you would with any other Drupal module.
2. Navigate to the Views configuration and locate the multi-value field
   settings.
3. Untick the 'Display all values in the same row' checkbox.
4. Add a Filter in the Last Delta category corresponding to the desired field.
5. Enable filtering by selecting 'Yes'.

With these settings, the View will only display the last value of the
multi-value field, making it easier to focus on the most recent and relevant
information.

## Requirements

- Drupal 8 or 9
- Views module

## Installation

1. Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.
2. Enable the module through the Drupal administration interface or via Drush
   (`drush en vfld`).

## Support & Maintenance

For any issues or feature requests, please use the [issue tracker](https://www.drupal.org/project/issues/vfld) on
the project page.

## Maintainer

Michal Gow (seogow) - https://www.drupal.org/u/seogow
