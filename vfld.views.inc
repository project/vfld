<?php

/**
 * @file
 * Views integration for the vfld module.
 */

/**
 * Implements hook_views_data_alter().
 */
function vfld_views_data_alter(array &$data): void {
  foreach ($data as $table_name => $column) {
    foreach ($column as $column_name => $column_data) {
      if (($column_name == 'delta') && isset($column['delta']['filter']['field_name'])) {
        $data[$table_name][$column_name . '_vfld'] = [
          'title' => t('@label (last)', ['@label' => (string) $column['delta']['title']]),
          'group' => t('Last Delta'),
          'filter' => [
            'field' => 'vfld',
            'table' => $table_name,
            'id' => 'vfld',
            'additional fields' => [],
            'field_name' => $column['delta']['filter']['field_name'],
            'entity_type' => $column['delta']['filter']['entity_type'],
            'allow empty' => TRUE,
          ],
        ];
      }
    }
  }
}
