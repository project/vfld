<?php

namespace Drupal\vfld\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Annotation\ViewsFilter;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter to handle last delta value.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("vfld")
 *
 * @package Drupal\vfld\Plugin\views\filter
 */
class LastDeltaFilter extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  public $always_required = TRUE;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new instance of LastDeltaFilter.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * The value options for the filter.
   *
   * @var array
   */
  protected array $valueOptions = [];

  /**
   * Returns options for this filter.
   */
  public function getValueOptions(): void {
    if (empty($this->valueOptions)) {
      $this->valueOptions = [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if ($this->isAGroup()) {
      return $this->t('grouped');
    }

    if (empty($this->valueOptions)) {
      $this->getValueOptions();
    }

    return $this->operator . ' ' . $this->valueOptions[!empty($this->value)];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExposeOptions() {
    parent::defaultExposeOptions();
    $this->options['expose']['operator_id'] = '';
    $this->options['expose']['label'] = $this->t('True');
    $this->options['expose']['required'] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function operatorOptions($which = 'title'): array {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  /**
   * Returns an array of operator information.
   *
   * @return array
   *   Returns operators for filter.
   */
  protected function operators(): array {
    return [
      '=' => [
        'title' => $this->t('Is last delta'),
        'method' => 'opQuery',
        'short' => $this->t('='),
        'values' => 1,
        'query_operator' => '=',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['value']['default'] = 0;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    if (empty($this->valueOptions)) {
      // Initialize the array of possible values for this filter.
      $this->getValueOptions();
    }

    if ($exposed = $form_state->get('exposed')) {
      // Exposed filter: use a select box to save space.
      $filter_form_type = 'select';
    }
    else {
      // Configuring a filter: use radios for clarity.
      $filter_form_type = 'radios';
    }

    $form['value'] = [
      '#type' => $filter_form_type,
      '#options' => $this->valueOptions,
      '#default_value' => $this->value,
    ];
    if (!empty($this->options['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      $user_input = $form_state->getUserInput();
      if ($exposed && !isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value;
        $form_state->setUserInput($user_input);
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function query() {
    if (!$this->value) {
      return;
    }

    $this->ensureMyTable();

    // Determine base tables and field.
    $database = \Drupal::database();
    $field = $database->escapeField("$this->tableAlias.delta");
    $query_base_table = $this->relationship ?: $this->view->storage->get('base_table');
    $entity_type = $this->entityTypeManager->getDefinition($this->getEntityType());
    $keys = $entity_type->getKeys();
    $data = Views::viewsData()->get($this->table);
    $join_info = $data['table']['join'][$query_base_table];

    // Extract table and field names from join information.
    $join_info_field = $join_info['field'];
    $query_base_table_id = $query_base_table . '.' . $keys['id'];

    // Create a subquery that selects the maximum delta value.
    $subquery = \Drupal::database()->select($this->tableAlias, 'subquery');
    $subquery->addExpression("MAX(subquery.delta)");
    $subquery->where("subquery.$join_info_field = $query_base_table_id");

    // Create an OR condition to include records either where the delta is NULL
    // or matches the max delta value.
    $or_condition = $this->query->getConnection()->condition('OR');
    $or_condition->condition($field, $subquery, 'IN');
    $or_condition->isNull($field);

    // Apply the constructed OR condition to the main query.
    $this->query->addWhere($this->options['group'], $or_condition);
  }

}
